all:
	cmake -S . -B build
	make -C build/ package
.PHONY: all

clean:
	make -C build/ clean
	rm -rf build
.PHONY: clean