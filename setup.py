#!/usr/bin/env python3
from setuptools import find_packages, setup

setup(
    name="mypackagedapp",
    packages=find_packages(exclude="__pycache__"),
    version="0.1",
    description="My Packaged Python App from CPack",
    install_requires = [
        'wheel' # Just an example pure-python dep
    ]
)