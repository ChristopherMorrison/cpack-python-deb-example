# CPack Python/Zipapp Deb Example
This is an example of the bare minimum it took for me to make a mostly self-contained python application that is distributable as a debian package.

## Some Thoughts
1. Replacing the zipapp binary with another chroot style format (appimage).

    This can be done with the appimage format for example to include native code for the platform such as MySQL client connector libraries.

2. Replacing the zipapp binary with a cgroups/container style format (flatpak).

    For flatpak this could be either done through the offline installer method (which would be very large) or the remote package reference (which would be more sane). The deb  package would then post-install script this to be installed by flatpak which would be slow but possible. At that point you may as well just use flatpak infrastructure. This experience could also be improved by adding wrapper scripts for the corresponding flatpak invocations for the app.

    For snap similar methods could probably be used although snap has less support/documentation so it wouldn't be as viable a method.

3. Multi-entrypoint projects

    This can be addressed in two ways; multiple-binaries or multiplexed entrypoints. For multiple-binaries multiple subprojects are built into separate binaries as is common with most native packages. For multiplexed entrypoints, the single binary is symlinked with the other binary names and the real binary runs different internal sub-entrypoints based on the file name. This is how busybox works. The multiplexed entrypoints method would also be more appropriate with a flatpack wrapping package as well.

4. Youtube-DL

    Youtube-DL happends to use a very similar packaging method to this. It is distributed a a zipapp and it seems the (out of date) ubuntu apt version was packaged in a very similar way.
